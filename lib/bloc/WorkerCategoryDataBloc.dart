import 'package:mobile_errol/model/remote/requests/WorkerCategoryRequest.dart';
import 'package:mobile_errol/model/remote/response/WorkerCategoryDataResponse.dart';
import 'package:mobile_errol/model/remote/util/ApiResponse.dart';
import 'package:mobile_errol/model/repo/WorkerCategoryDataRepository.dart';
import 'package:rxdart/rxdart.dart';

class WorkerCategoryDataBloc {
  final WorkerCategoryDataRepository _paymentPlanRepository =
      WorkerCategoryDataRepository();

  final BehaviorSubject<ApiResponse<WorkerCategoryDataResponse>>
      _subjectWorkerCategoryData =
      BehaviorSubject<ApiResponse<WorkerCategoryDataResponse>>();

  executeWorkerCategoryData(WorkerCategoryRequest workerCategoryRequest) {
    _paymentPlanRepository.executeWorkerCategoryData(
        workerCategoryRequest, _subjectWorkerCategoryData);
  }

  BehaviorSubject<ApiResponse<WorkerCategoryDataResponse>> get subject =>
      _subjectWorkerCategoryData;

  /// functions that used to  close the Subject stream
  disposeWorkerCategoryDataSubject() {
    _subjectWorkerCategoryData.close();
  }
}
