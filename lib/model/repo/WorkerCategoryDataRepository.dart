import 'package:mobile_errol/model/remote/requests/WorkerCategoryRequest.dart';
import 'package:mobile_errol/model/remote/response/WorkerCategoryDataResponse.dart';
import 'package:mobile_errol/model/remote/util/ApiResponse.dart';
import 'package:rxdart/rxdart.dart';

import 'calls/WorkerCategoryDataCall.dart';

class WorkerCategoryDataRepository {
  executeWorkerCategoryData(
      WorkerCategoryRequest workerCategoryRequest,
      BehaviorSubject<ApiResponse<WorkerCategoryDataResponse>>
          responseSubject) {
    new WorkerCategoryDataCall(workerCategoryRequest, responseSubject)
        .execute();
  }
}
