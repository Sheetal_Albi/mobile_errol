class WorkerCategoryData {
  bool status;
  List<WorkerData> dataList;

  WorkerCategoryData({this.status, this.dataList});

  WorkerCategoryData.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    dataList = new List<WorkerData>();
    json['data'].forEach((v) {
      dataList.add(new WorkerData.fromJson(v));
    });
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.dataList != null) {
      data['data'] = this.dataList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class WorkerData {
  int workertypeId;
  String workerType;

  WorkerData({this.workertypeId, this.workerType});

  WorkerData.fromJson(Map<String, dynamic> json) {
    workertypeId = json['Workertype_id'];
    workerType = json['Worker_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Workertype_id'] = this.workertypeId;
    data['Worker_type'] = this.workerType;
    return data;
  }
}
