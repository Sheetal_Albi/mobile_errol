import 'dart:core';

import '../ApiConstants.dart';

class WorkerCategoryRequest {
  int id;

  WorkerCategoryRequest(this.id);

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = new Map();
    map[ApiConstants.ID] = id;
    print("WorkerCategoryRequest: " + map.toString());
    return map;
  }
}
