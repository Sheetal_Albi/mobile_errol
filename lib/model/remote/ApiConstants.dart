class ApiConstants {
  static final String BASE_URL = "http://www.albiorixtech.com/hepa/api/";

  //api name
  static final String WORKER_CATEGORY_DATA = "/workerCategoryDataNew";

  // parameter
  static final String ID = 'id';
}
