import 'package:mobile_errol/model/beans/WorkerCategoryData.dart';

import 'GeneralResponse.dart';

class WorkerCategoryDataResponse extends GeneralResponse {
  WorkerCategoryData Result;

  WorkerCategoryDataResponse.fromJson(Map<String, dynamic> json)
      : Result = WorkerCategoryData.fromJson(json),
        super.fromJson(json);
}
