import 'package:dio/dio.dart';
import 'package:mobile_errol/model/remote/requests/WorkerCategoryRequest.dart';

import '../ApiConstants.dart';
import 'NetworkUtil.dart';

/// write your all API Async requests here
class ApiService {
  NetworkUtil networkUtil = NetworkUtil();

  // Worker category data Details
  Future<Response> workerCategoryData(
      WorkerCategoryRequest workerCategoryRequest) {
    return networkUtil.post(
        ApiConstants.WORKER_CATEGORY_DATA, workerCategoryRequest.toMap());
  }
}

///Single final Object of API Service
final apiServiceInstance = ApiService();
